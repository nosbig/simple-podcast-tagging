#!/bin/bash
#set -e
#set -x

export IMAGEPATH=/home/user/images/podcast-cover-art-2018-07-29.png
export FILENAME=dummy
export IMAGEROOT=$(pwd)/coverart
export AUDIO_SERIES="Sunday Services"
export ORGANIZATION="Church"
export RELEASE_DATE=$(date -Idate)
export YEAR=$(date -Idate | cut -f 1 -d-)
export COMMENT="(c) 2018 - Church"

export CONFIRM=N

function getinput()
{
read -p 'Presenter: ' PRESENTER
read -p 'Title: ' TITLE
read -p 'Release Date: ' RELEASE_DATE

echo '1. Sunday Services'
#echo '2. Religious Education'
#echo '3. StoryCorps'
read -p 'Which show are we processing? ' SHOW

case $SHOW in
        1)
                AUDIO_SERIES="Sunday Services $YEAR"
                ORGANIZATION="Church"
                COMMENT="(c) 2019 - Church"
                IMAGEPATH="$IMAGEROOT/sunday-service.png"
                echo $IMAGEROOT
                ;;
        2)
                AUDIO_SERIES="Religious Education"
                ORGANIZATION="Church"
                COMMENT="(c) 2019 - Church"
                ;;
        3)
                AUDIO_SERIES="StoryCorps"
                ORGANIZATION="Church"
                COMMENT="(c) 2019 - Church"
                ;;
        *)
                echo 'Please Enter a Valid Show Workflow'
                exit 1
                ;;
esac
}

until [ -f $FILENAME ] && [ $(echo $FILENAME | tail -c 5) = ".mp3" ]
do
        read -p 'File to update: ' FILENAME
done

until [ $CONFIRM = 'Y' ] || [ $CONFIRM = 'y' ]
do
        getinput
        echo 'Podcast Details'
        echo "Filename: $FILENAME"
        echo "Show: $SHOW"
        echo "Presenter: $PRESENTER"
        echo "Title: $TITLE"
        echo "Release Date: $RELEASE_DATE"
        echo ''
        echo 'Are these the correct details to write to the file? [y/n]'
        read CONFIRM
done

eyeD3 $FILENAME --add-image "$IMAGEPATH:FRONT_COVER" --release-date "$RELEASE_DATE" -a "$PRESENTER" -A "$AUDIO_SERIES" -b "$ORGANIZATION" -t "$TITLE" -Y "$YEAR" -c "$COMMENT"
